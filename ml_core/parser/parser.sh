#!/bin/bash

bison -d parser.y

if [ $1 -eq 1 ]
then
g++ -D__MW_TOOLKIT__ -D__MW_COMPLEX_MSG_OUTPUT__  -std=c++20 -o ml_parser parser.cpp ../scanner/scanner.cpp -lreflex
fi
