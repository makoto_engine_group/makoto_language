/*******************************************************************************
*                                                                              * 
*                Copyright 2020 Makoto World - Talles HBF                      *
*                                                                              *
*        Licensed under the Apache License, Version 2.0 (the "License");       *
*       you may not use this file  except in compliance with the License.      *
*                 You may obtain a copy of the License at                      *
*                                                                              *
*               http://www.apache.org/licenses/LICENSE-2.0                     *
*                                                                              *
*     Unless required by applicable law or  agreed to in writing, software     *
*       distributed under the License is distributed on an "AS IS" BASIS,      *
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *
*     See the License for the specific language governing permissions  and     *
*                      limitations under the License.                          *
*                                                                              *
*******************************************************************************/ 
/*******************************************************************************
*  Author: Talles HBF - Kourei                                                 *
*  Collab: <empty>                                                             *
*******************************************************************************/  
%require "3.4"
%language "c++"


%define api.namespace {yy}
%define api.parser.class {parser}
%define api.value.type variant
%define api.token.constructor
%define parse.error verbose

%defines
%output "parser.cpp"

%locations
%define api.location.file "location.hpp"

%parse-param { yy::scanner& lexer }

%define api.token.prefix {TOK_}
%token <std::string> TYPE  "Var type"
%token <std::wstring> STR  "String";
%token <std::wstring> VAR  "Variable";
%token <double>       NUM  "Number";
%token <signed>       INT  "Integer";
%token EOF             0   "End of file";
%token <std::string>  IF   "IF stmt";
%token <std::string>  ELIF "Else if statement"
%token <std::string>  ELSE "Else statement"
%token <std::string>  END_STMT "End statment";
%token <char> R_PAR;
%token <char> L_PAR;
%token <char> PLUS;
%token <char> MINUS;
%token <char> MULT;
%token <char> DIV;
%token '=';
%token '(';
%token ')';
%token ';';
%token <char>SPR;
%token <std::string> NOT
%token <std::string> AND
%token <std::string> OR
%token ID;

%right '='
%left  MINUS PLUS
%left  MULT DIV
%left  AND OR
%right NOT NEG

%nterm <long double> expr
%nterm <bool> cond
%nterm decl

%code requires {
  namespace yy {
    class scanner;
  };
}

%code{
  #include "../scanner/scanner.hpp"
  #include <iostream>
  #include <fstream>
  #undef  yylex
  #define yylex lexer.lex  // Within bison's parse() we should invoke lexer.yylex(), not the global yylex()
}

%%


stmt : stmt ifstmt
     | stmt decl '.'
     | stmt calc '.'
     | stmt '.'         { lexer.out() << "? "; }
     | error '.'        { yyerrok; }
     |
     ;

block   : stmt MINUS
        ;

else_stmt : ELSE block
          ;

elif_stmt : ELIF cond SPR block
          | ELIF cond SPR stmt elif_stmt
          | ELIF cond SPR stmt else_stmt
          ;

ifstmt  : IF SPR cond SPR block 
        | IF SPR cond SPR stmt else_stmt
        | IF SPR cond SPR stmt elif_stmt 
        ;

decl  : TYPE VAR           { lexer.new_attr($1, $2); }
      | TYPE VAR '=' expr  { lexer.new_attr($1, $2, $4); }
      ;

calc : VAR '=' expr   { lexer.update_attr($1, $3); }
     ;

cond : expr {$$ = $1; }
     ;

expr:   expr MINUS expr     { $$ = $1 - $3; }
      | expr PLUS  expr     { $$ = $1 + $3; }
      | expr MULT expr      { $$ = $1 * $3; }
      | expr DIV expr       { $$ = $1 / $3; }
      | expr OR  expr       { $$ = $1 || $3; }
      | MINUS %prec NEG expr  { $$ = -$2; }
      | '(' expr ')'        { $$ = $2; }
      | NOT expr            { $$ = !$2; }
      | VAR                 { $$ = lexer.get_var($1); }
      | INT                 { $$ = $1; }
      | NUM                 { $$ = $1; }
      ;

%%


/* Temporay  - Re-flex Examples Code  <<  https://github.com/Genivia/RE-flex/blob/master/examples/*/
void yy::parser::error(const location& loc, const std::string& msg)
{
  std::cerr << loc << ": " << msg << std::endl;
  if (loc.begin.line == loc.end.line && loc.begin.line == lexer.lineno())
  {
    std::cerr << lexer.matcher().line() << std::endl;
    for (size_t i = 0; i < loc.begin.column; ++i)
      std::cerr << " ";
    for (size_t i = loc.begin.column; i <= loc.end.column; ++i)
      std::cerr << "~";
    std::cerr << std::endl;
  }
  else
  {
    FILE *file = lexer.in().file(); // the current file being scanned
    if (file != NULL)
    {
      yy::scanner::Matcher *m = lexer.new_matcher(file); // new matcher
      lexer.push_matcher(m); // save the current matcher
      off_t pos = ftell(file); // save current position in the file
      fseek(file, 0, SEEK_SET); // go to the start of the file
      for (size_t i = 1; i < loc.begin.line; ++i)
        m->skip('\n'); // skip to the next line
      for (size_t i = loc.begin.line; i <= loc.end.line; ++i)
      {
        std::cerr << m->line() << std::endl; // display the offending line
        m->skip('\n'); // next line
      }
      fseek(file, pos, SEEK_SET); // restore position in the file to continue scanning
      lexer.pop_matcher(); // restore matcher
    }
  }
  if (lexer.size() == 0) // if token is unknown (no match)
    lexer.matcher().winput(); // skip character
}

int main(int argc, char **argv)
{
  if (argc < 2)
    return EXIT_FAILURE;

  FILE *file = fopen(argv[1], "r");

  if (file == NULL)
  {
    perror("Cannot open file");
    return EXIT_FAILURE;
  }

  yy::scanner scanner(file, std::cout);
  yy::parser  parser(scanner);
  return parser.parse();
}
