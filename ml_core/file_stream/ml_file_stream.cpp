#ifndef ML_CORE_FSTREAM_ML_FILE_TYPE_CPP_
#define ML_CORE_FSTREAM_ML_FILE_TYPE_CPP_

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstdint>
#include <functional>


#include "ml_file_stream.hpp"


size_t hash;
size_t col_size;
size_t row_size;
int8_t type;
    
char*  title;
char*  path;
  
    // Functions
void  mset_col_size();
void  set_row_size();
void  set_path( char * path );
    
  
    
void   set_file( std::fstream& file );
    
size_t get_col_size();
size_t get_row_size();
char*  get_path();
char*  get_name();



#endif // ML_CORE_ML_FILE_TYPE_
