#ifndef ML_CORE_FSTREAM_OFILE_CPP_
#define ML_CORE_FSTREAM_OFILE_CPP_


enum ML_FILE_TYPE {
  ML_FILE_ERROR = -1,
  ML_MAIN       =  0,
  ML_SCENE      =  1,
  ML_CLASS      =  2,
  ML_HEADER     =  3
}


class ofile : public std::ofstream
{
  public:
    std::ofstream file;
  
  
    ofile();
    ~ofile();
    

    
    size_t get_col_size();
    size_t get_row_size();
    char*  get_path();
    char*  get_name();
};


class ifile : public std::wifstream
{
  public:
    ifile() : std::wifstream() ;
    ~ifile(); ~std::wifstream() ;
    
    bool  open( const char* filename );
    /*
        {
        std::wifstream::open( filename, ios_base::in );
        
        if(is_open())
          return true;
        return false
        }
    */
    
    
};



#endif // ML_CORE_FSTREAM_OFILE_CPP_
