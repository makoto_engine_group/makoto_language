#ifndef ML_CORE_FSTREAM_ML_FILE_TYPE_CPP_
#define ML_CORE_FSTREAM_ML_FILE_TYPE_CPP_

#include "../mw-toolkit/mw_toolkit.h"

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cstdint>
#include <functional>
#include "ml_fstream.hpp"
#include "ml_fstream_enum.hpp"





bool
ifile::open( const char* filename )
  {
  std::wifstream::open( filename, ios_base::in );
        
  if(is_open())
    {
    mw_smsg("File is opened.");
    
    seekg (0, end );
    lenght(tellg());
    seekg (0, beg );
    
    
    mw_mem_verbose("In Open File State",{
      int16_t lenght_ = lenght();
      _(lenght_)
    })
    
    return true;
    }
  mw_serror("File is not opened.");
  return false;
  }
        
#endif // ML_CORE_ML_FILE_TYPE_
