#ifndef ML_CORE_FILE_STREAM_ML_FILE_DATA_HPP
#define ML_CORE_FILE_STREAM_ML_FILE_DATA_HPP





enum ML_FILE_TYPE {
  ML_FILE_ERROR = -1,
  ML_MAIN       =  0,
  ML_SCENE      =  1,
  ML_CLASS      =  2,
  ML_HEADER     =  3
}

class data
{
  private:
    size_t hash;
    size_t col_size;
    size_t row_size;
    int8_t type;
    
    char*  title;
    char*  path;
  
    // Functions
    void  set_col_size( size_t size );
    void  set_row_size();
    void  set_path( char * path );
    
  
  public:
    MLFile();
    ~MFFile();
    
    void   set_file( std::fstream& file );
    
    size_t get_col_size();
    size_t get_row_size();
    char*  get_path();
    char*  get_name();
};



#endif // ML_CORE_FILE_STREAM_ML_FILE_DATA_HPP
