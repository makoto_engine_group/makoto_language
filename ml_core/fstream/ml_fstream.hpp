#ifndef ML_CORE_FSTREAM_OFILE_CPP_
#define ML_CORE_FSTREAM_OFILE_CPP_



class ofile : public std::wofstream
{
  public:
    std::ofstream file;
  
  
    ofile();
    ~ofile();
    

    
    size_t get_col_size();
    size_t get_row_size();
    char*  get_path();
    char*  get_name();
};


class ifile : public std::wifstream
{    
  mk_auto_io(int8_t , type  )
  mk_auto_io(int16_t, lenght)
   
  public:  
  bool  
  open( const char* filename );
};



#endif // ML_CORE_FSTREAM_OFILE_CPP_
