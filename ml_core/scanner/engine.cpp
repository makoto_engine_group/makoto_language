#ifndef ML_CORE_SCANNER_ENGINE_CPP_
#define ML_CORE_SCANNER_ENGINE_CPP_

#include <stdio.h>
#include <iostream>
#include <cstdlib>
#include <cstdint>

#include "../common/mk_verbose/verbose_header.h"
#include "token.hpp"
#include "engine.hpp"


using namespace std;


int32_t Scanner::engine::var_size(int32_t var_type, uint8_t *l_char)
{
  
  switch(*l_char)
    {
    case '8': { if ( (*l_char = getchar()) == '0' && var_type == TOK_FLT ) return(TOK_FLT + TOK_80); else if( var_type == TOK_DIG ) return(TOK_DIG + TOK_8); break;}
    case '1': { if ( (*l_char = getchar()) == '6' ) return(var_type + TOK_16); break;}
    case '3': { if ( (*l_char = getchar()) == '2' ) return(var_type + TOK_32); break;}
    case '6': { if ( (*l_char = getchar()) == '4' ) return(var_type + TOK_64); break;}
    }
    
    mk_serror("Var Scanner error");
    return EXIT_FAILURE;

    //Verificar com Valgrind se precisa desalocar
}

void Scanner::engine::mount_alpha( std::string str, char value)
{
    return;
}



int32_t Scanner::engine::get_token()
{
  static uint8_t last_char = ' ';
  
  while(isspace(last_char)) last_char = getchar();
  
  
  if(ispunct(last_char) && (last_char == '@'))
    {
    if(isdigit(last_char))
      return EXIT_FAILURE;
    
    while(isalnum(last_char = getchar()))
      idntfr_string += last_char;
      
    return TOK_FUNC;
    } 
     
  if(ispunct(last_char) && (last_char == '$'))
    {
    if(isdigit(last_char))
      return EXIT_FAILURE;
    
    while(isalnum(last_char = getchar()))
      idntfr_string += last_char;
      
    return TOK_MTHD;
    }
  
  if(isalpha(last_char))
    {   
    idntfr_string = last_char;
     
    while(isalpha( last_char = getchar() )) idntfr_string += last_char;

    //Verificadao do Tipo da Variavel
    if(idntfr_string == "int"  ) 
      {
      last_char = getchar();
      return Scanner::engine::var_size( TOK_DIG,   &last_char );
      }
    else
    if(idntfr_string == "float") 
      {
      last_char = getchar();
      return Scanner::engine::var_size( TOK_FLT, &last_char );
      }
    }
  
  return EXIT_FAILURE;
  //if(isdigit(last_char))
}



#endif // ML_CORE_SCANNER_ENGINE_CPP_
