#pragma once

#ifndef float32_t
    #define float32_t float
#endif

#ifndef float64_t
    #define float64_t double
#endif



#ifndef ML_CORE_TOKEN_TOKEN_HPP_
#define ML_CORE_TOKEN_TOKEN_HPP_


typedef struct
token_u32{
    int32_t id;
    union {
        int32_t  INTEGER;
        float32_t  FLOAT;
    } value ;
} Token_U32;

typedef struct
token_u64{
    int32_t id;
    union {
        int64_t   INTEGER;
        float64_t FLOAT;
    } value ;
} Token_U64;
  
  
  
 
enum
TOK
{
  TOK_EOF = -1,
  
  /*
  TOK_INCLUDE
  TOK_EXTERN_LIB
  TOK_INTERN_LIB
  
  TOK_EVENT
  TOK_STATIC
  TOK_DYNAMIC
  TOK_LABEL
  TOK_GOTO
  
  TOK_COND_IF
  TOK_COND_EI
  TOK_COND_EL
  
  TOK_SWITCH
  TOK_CASE_BRK  
  TOK_CASE_NBRK
  TOK_CASE_DFLT
  
  TOK_BREAK
  TOK_CONTINUE
  
  TOK_SUM
  TOK_MIN
  TOK_DIV
  TOK_MULT
  
  TOK_OR
  TOK_AND
  TOK_XOR
  TOK_NOR
  TOK_NAND
  TOK_NXOR
  
  */
  
  TOK_CHR = -2,
  TOK_STR = -3,
  TOK_DIG = -10,
  TOK_FLT = -20,
  
  TOK_FUNC= -6,
   
  TOK_CELL= -50,
  TOK_OBJ = -60,
  TOK_MTHD= -1,
  TOK_ATTB= -2,
  TOK_PUBL= -3,
  TOK_PRIV= -4,
  TOK_PROT= -5,
  
  TOK_8   = -90,
  TOK_16  = -91,
  TOK_32  = -92,
  TOK_64  = -93,
  TOK_80  = -94,
  
  TOK_DIG_8  = -100,
  TOK_DIG_16 = -101,
  TOK_DIG_32 = -102,
  TOK_DIG_64 = -103,
  
  TOK_FLT_8  = -110,
  TOK_FLT_16 = -111,
  TOK_FLT_32 = -112,
  TOK_FLT_64 = -113,
  TOK_FLT_80 = -114,
  
};


static std::string idntfr_string;
static double      idmtfr_value;



#endif // ML_CORE_TOKEN_TOKEN_HPP_
