/*******************************************************************************
*                                                                              * 
*                Copyright 2020 Makoto World - Talles HBF                      *
*                                                                              *
*        Licensed under the Apache License, Version 2.0 (the "License");       *
*       you may not use this file  except in compliance with the License.      *
*                 You may obtain a copy of the License at                      *
*                                                                              *
*               http://www.apache.org/licenses/LICENSE-2.0                     *
*                                                                              *
*     Unless required by applicable law or  agreed to in writing, software     *
*       distributed under the License is distributed on an "AS IS" BASIS,      *
*   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   *
*     See the License for the specific language governing permissions  and     *
*                      limitations under the License.                          *
*                                                                              *
*******************************************************************************/ 
/*******************************************************************************
*  Author: Talles HBF - Kourei                                                 *
*  Collab: <empty>                                                             *
*******************************************************************************/  


%top{

#include "../parser/parser.hpp"
#include "../parser/location.hpp"
#include "../mw-toolkit/mw_toolkit.h"
#include <cstdlib>
#include <cstdint>
#include <type_traits>

namespace chk{
  template <typename T>
  struct type{static const int value = 0;};

  template <>
  struct type<signed int>{ static const int  value = 1; };

  template <>
  struct type<signed long>{ static const int value = 2;};
  

  
  }



%}


%class{
 public:
  signed attr_count = 0;
 
  std::map<std::wstring, std::string> hash_map;
  std::map<std::wstring, bool >  bool_map;
  std::map<std::wstring, long double>  flt80_map;
  std::map<std::wstring, double     >  flt64_map;
  std::map<std::wstring, float      >  flt32_map;
  std::map<std::wstring, uint64_t   > uint64_map;
  std::map<std::wstring, uint32_t   > uint32_map;
  std::map<std::wstring, uint16_t   > uint16_map;
  std::map<std::wstring, uint8_t    > uint8_map;
  std::map<std::wstring, int64_t    > int64_map;
  std::map<std::wstring, int32_t    > int32_map;
  std::map<std::wstring, int16_t    > int16_map;
  std::map<std::wstring, int8_t     > int8_map;
  std::map<std::wstring, char          > char_map;
  std::map<std::wstring, unsigned char > uchar_map;
  std::map<std::wstring, wchar_t       > wchar_map;
  std::map<std::wstring, std::string   > str_map;
  std::map<std::wstring, std::wstring  > wstr_map;

  union ml_swap{
    long double  ld;
    
    union {
      double    f;
      int64_t   i;
      uint64_t ui;
    } b64;
    
    union  {
      float     f;
      int32_t   i;
      uint32_t ui;
      wchar_t   w;
    } b32;
    
    union {
      int16_t   i;
      uint16_t ui;
    } b16;
    
    union {
      int8_t i;
      uint8_t ui;
    } b8;
  } swp;
  

  bool
  mw_cmp(const std::string& var, const std::string& type, std::size_t size)
    {
    if(var.compare(0, var.size() ,type)  == 0)
      return true; 
    return false;
    }

  /* Input Method */ 
  
  void 
  i32 ( std::wstring name, int32_t in )
    { int32_map[name] = swp.b32.i = in; }
  int32_t
  i32 ( std::wstring name )
    { return int32_map[name]; }

  
  void 
  i64 ( std::wstring name, int64_t in )
    { int64_map[name] = swp.b64.i = in; }
  int64_t
  i64 ( std::wstring name )
    { return int64_map[name]; }

  
  void
  f64 ( std::wstring name, double in ) 
    { flt64_map[name] = swp.b64.f = in; }
  double
  f64 ( std::wstring name )
    { return flt64_map[name]; }


  long double 
  get_var(std::wstring name)
    {
    if(int32_map.contains(name)) return (long double) i32(name);
    if(int64_map.contains(name)) return (long double) i64(name);
    if(flt64_map.contains(name)) return (long double) f64(name);
    std::cout << "Undefined Variable" << '\n'; 
    exit(EXIT_FAILURE);
    }




  bool
  mw_decl_bool(const std::string& c)
    { return mw_cmp(c, "bool", 4); }

  bool
  mw_decl_i8(const std::string& c)
    { return mw_cmp(c, "int8", 4);   } 

  bool
  mw_decl_i16(const std::string& c)
    { return mw_cmp(c, "int16", 5);  } 

  bool
  mw_decl_i32(const std::string& c)
    { return mw_cmp(c, "int32", 5);  } 

  bool
  mw_decl_i64(const std::string& c)
    { return mw_cmp(c, "int64", 5);  } 
    
  bool
  mw_decl_f64(const std::string& c)
    { return mw_cmp(c, "float64", 7);} 



  /* NEW_ATTR */

  template <typename F> bool
  new_attr(std::string type, std::wstring name, F expr)
    {
    if(hash_map.contains(name))
      {
      mw_serror("Duplicated Attribute/Variable");
      goto new_failure;
      }
    
    if(mw_decl_i32(type))
      {
      i32(name, expr);
      std::cout << "=> " << i32(name) << '\n';
      goto new_sucess;
      }  
     
    if(mw_decl_i64(type))
      {
      i64(name, expr);
      std::cout << "=> " << i64(name) << '\n';
      goto new_sucess;
      }
     
    if(mw_decl_f64(type))
      {
      f64(name, expr);
      std::cout << "=> " << f64(name) << '\n';
      goto new_sucess;
      }     

    mw_serror("Undefined Type Declaration");
       
    new_failure:
    mw_serror("Killing the Process.");
    mw_mem_verbose("New Attribute Failure Exist::log",
      _(type)
      _(expr)
    )
    exit(EXIT_FAILURE);
   
    new_sucess:
    hash_map.insert({name, type});
    std::wcout << L"Added new var [[ " << name << " : ";
    std::cout  << type << " ]]\n"; 
    return true;
   }

  bool
  new_attr(std::string type, std::wstring name)
    {
    if(hash_map.contains(name))
      {
      mw_serror("Duplicated Attribute/Variable");
      goto new_failure;
      }
    
    if(mw_decl_i32(type))
      {
      i32(name, 0);
      std::cout << "=> " << i32(name) << '\n';
      goto new_sucess;
      }  
     
    if(mw_decl_i64(type))
      {
      i64(name, 0);
      std::cout << "=> " << i64(name) << '\n';
      goto new_sucess;
      }
     
    if(mw_decl_f64(type))
      {
      f64(name, 0);
      std::cout << "=> " << f64(name) << '\n';
      goto new_sucess;
      }     

    mw_serror("Undefined Type Declaration");
       
    new_failure:
    mw_serror("Killing the Process.");
    mw_mem_verbose("New Attribute Failure Exist::log",
      _(type)
    )
    exit(EXIT_FAILURE);
   
    new_sucess:
    hash_map.insert({name, type});
    std::wcout << L"Added new var [[ " << name << " : ";
    std::cout  << type << " ]]\n"; 
    return true;
   }
   

  /* UPDATE_ATTR */

  template <typename F> bool
  update_attr(std::wstring name, F expr)
    {
    std::string ml_deck_type(hash_map[name]);
    if(!hash_map.contains(name))
      {
      mw_serror("This Attribute/Variable does not exist.");
      goto update_failure;
      }
  
    if(mw_decl_i32(ml_deck_type))
      {
      int32_map[name] = swp.b32.i = expr;
      std::cout << "=> " << int32_map[name] << '\n';
      return EXIT_SUCCESS;
      }
  
    if(mw_decl_i64(ml_deck_type))
      { 
      int64_map[name] = swp.b64.i = expr;
      std::cout << "=> " << int64_map[name] << '\n';
      return EXIT_SUCCESS;
      }
  
    if(mw_decl_f64(ml_deck_type))
      {
      f64(name, expr);
      std::cout << "flt64=> " << f64(name) << '\n';
      return EXIT_SUCCESS;
      }
     
    update_failure:
    mw_serror("Killing the Process.");
    mw_mem_verbose("Update Attribute Failure Exist::log",
      _(expr)
    )
    exit(EXIT_FAILURE);
    }
}

%option bison-complete
%option bison-cc-namespace=yy
%option bison-cc-parser=parser
%option bison-locations

%option exception="yy::parser::syntax_error(location(), \"Unknown token.\")"

%option namespace=yy
%option lexer=scanner
%option outfile=scanner.cpp
%option header-file=scanner.hpp


%option fast
%option unicode
//%option freespace


IF   if
ELSE (-[\s]*:)
ELIF (\+[\s]*:)
SPR  ":"



SINT int(8|16|32|64)
SFLT float(16|32|64|80)

VAR  \p{UnicodeIdentifierStart}\p{UnicodeIdentifierPart}*
INT  \d+
EXP  [Ee][-+]?\d+
FLT  \d*(\d|\.\d)\d*{EXP}?
OP   [=.]
SMB  (=|equal|:<--)
MINUS "-"
PLUS  "+"
MULT  "*"
DIV   "/"
R_PAR "("
L_PAR ")"
NOT ("NOT"|!)
AND ("AND"|&&)
OR  ("OR"|"||")

%%

\s             // ignore space
\/:(.|\n)*?:\/ // Ignore comments
\/\/.*\n       // Ignore comments
\"[^"]*\"  { return yy::parser::make_STR(wstr(), location());}
{ELSE}     { return yy::parser::make_ELSE("elsestmt", location());}
{ELIF}     { return yy::parser::make_ELIF("elifstmt", location());}
{IF}       { return yy::parser::make_IF(str(), location());}
{R_PAR}    { return yy::parser::make_R_PAR(chr(), location());}
{L_PAR}    { return yy::parser::make_L_PAR(chr(), location());}
{MINUS}    { return yy::parser::make_MINUS('m', location());}
{PLUS}     { return yy::parser::make_PLUS('p', location());}
{MULT}     { return yy::parser::make_MULT('x', location());}
{DIV}      { return yy::parser::make_DIV('d', location());}
{NOT}      { return yy::parser::make_NOT(str(), location());}
{AND}      { return yy::parser::make_AND(str(), location());} 
{OR}       { return yy::parser::make_OR( str(), location());} 
{SINT}     { return yy::parser::make_TYPE(str(), location());}
{SFLT}     { return yy::parser::make_TYPE(str(), location());}
{VAR}      { return yy::parser::make_VAR(wstr(), location());}
{SPR}      { return yy::parser::make_SPR(chr(), location());}
{INT}      { return yy::parser::make_NUM(strtod(text(), NULL), location());}
{FLT}      { return yy::parser::make_NUM(strtod(text(), NULL), location());}
{OP}       { return yy::parser::symbol_type(chr(), location()); }
<<EOF>>    { return yy::parser::make_EOF(location()); }
%%
