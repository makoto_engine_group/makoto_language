
#ifndef ML_CORE_SCANNER_ENGINE_HPP_
#define ML_CORE_SCANNER_ENGINE_HPP_

#include "scanner.ns"

class Scanner::engine
{
  
  public:
    //Scanner();
    //~Scanner();
    
    int32_t get_token();
    
  private:
    int count;
    void    mount_alpha( std::string str , char value);
    int32_t var_size(int32_t var_type, uint8_t *l_char);
    
    
};




#endif // ML_CORE_SCANNER_ENGINE_HPP_
