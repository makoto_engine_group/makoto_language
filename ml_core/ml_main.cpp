#ifndef ML_CORE_ML_MAIN_CPP
#define ML_CORE_ML_MAIN_CPP

#include <iostream>
#include <cstdlib>

#include "mw-toolkit/mw_toolkit.h"
#include "scanner/engine.hpp"
#include "parser/parser.hpp"


int
main(int argc, char **argv)
{
    MW_INIT_VERBOSE();

    Scanner::engine *s = new Scanner::engine();
    std::cout << s->get_token() << std::endl;

    return EXIT_SUCCESS;
}




#endif // ML_CORE_ML_MAIN_CPP
